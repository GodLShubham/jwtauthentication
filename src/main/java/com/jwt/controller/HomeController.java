package com.jwt.controller;

import org.apache.coyote.http11.Http11AprProtocol;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class HomeController {
	
	@GetMapping("/welcome")	
//	public ResponseEntity<?> welcome() {
		public String welcome() {
		
		String text ="this is he welcome controller being called";
		System.out.println("Inside Welcome");
//		return new ResponseEntity<>(text,HttpStatus.OK);
		return text;
	}
	
}
