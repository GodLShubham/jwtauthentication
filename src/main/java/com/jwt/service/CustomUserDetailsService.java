package com.jwt.service;

import java.util.ArrayList;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService{
	
//	This method is used to fetch username and password from database to be compared with username and password from FE 
	@Override
		public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
			// TODO Auto-generated method stub
			if(username.equals("Durgesh")) {
				return new User("Durgesh", "Durgesh123", new ArrayList<>());
			}else {
				System.out.println("this user does not exist....");
				throw new UsernameNotFoundException("The given username does not exist   !!!");
			}
			
		}	
	
}
